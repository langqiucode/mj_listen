# 麻将听牌提示（附加测试工具）

#### 介绍
目前是针对一款麻将做达到番数才有的听牌提示，附带网页测试工具。

#### 使用

测试工具是web版本 需要起一个服务 访问Index.html

tsrsc 是听牌逻辑的ts代码 需要使用tsc编译 

yarn 或者 npm 安装依赖包

#### 使用说明

牌值对应：

万：0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09

条：0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19

筒：0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29

字：0x31,0x32,0x33,0x34,0x35,0x36,0x37

![输入图片说明](https://images.gitee.com/uploads/images/2019/0813/144330_2304f68f_5138898.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0813/144339_2379241a_5138898.png "2.png")

初次提交，代码还是有点粗糙，不算优美，欢迎各位大佬指点。

有什么疑问可以联系我 WX:yy88078